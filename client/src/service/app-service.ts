import { ACCESS_TOKEN } from './../modules/share/constants';
import { AccountDto } from '../models/account.model';
import { SchedulerDto } from './../models/scheduler.model';
import { TaskDto } from './../models/task.model';
import { ENDPOINTS, header } from '../modules/share/constants';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment';
import { ScheduleDto } from 'src/models/schedule.model';
import { LocalStorageDto } from 'src/models/localstorage.model';

@Injectable()
export class AppService {
  REST_API = environment.MY_END_POINT;
  token = this.getTokenInLocal();
  headerOptions = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Authorization', 'Bearer '.concat(this.token.token));
  constructor(protected http: HttpClient) { }

  getAllTask() {
    const url = this.REST_API + ENDPOINTS.getTasks;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  getAllScheduler() {
    const url = this.REST_API + ENDPOINTS.getSchedulers;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  getAllSchedule() {
    const url = this.REST_API + ENDPOINTS.getSchedules;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  getAllCategory() {
    const url = this.REST_API + ENDPOINTS.getCategories;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  addTask(taskDto: TaskDto) {
    const url = this.REST_API + ENDPOINTS.getTasks;
    return this.http.post(url, JSON.stringify(taskDto), { "headers": this.headerOptions });
  }

  specificTask(taskId: any) {
    const url = this.REST_API + ENDPOINTS.getTasks + '/' + taskId;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  deleteTask(taskId: any) {
    const url = this.REST_API + ENDPOINTS.getTasks + '/' + taskId;
    return this.http.delete(url, { "headers": this.headerOptions });
  }

  updateTask(task: TaskDto) {
    const url = this.REST_API + ENDPOINTS.getTasks;
    return this.http.put(url, task, { "headers": this.headerOptions });
  }

  goBack() {
    window.history.back();
  }

  confirmDialog(question: string) {
    return window.confirm(question);
  }

  addScheduler(scheduler: SchedulerDto) {
    const url = this.REST_API + ENDPOINTS.getSchedulers;
    return this.http.post(url, scheduler, { "headers": this.headerOptions });
  }

  updateScheduler(scheduler: SchedulerDto) {
    const url = this.REST_API + ENDPOINTS.getSchedulers;
    return this.http.put(url, scheduler, { "headers": this.headerOptions });
  }

  deleteScheduler(schedulerId: number) {
    const url = this.REST_API + ENDPOINTS.getSchedulers + '/' + schedulerId;
    return this.http.delete(url, { "headers": this.headerOptions });
  }

  addSchedule(schedule: ScheduleDto) {
    const url = this.REST_API + ENDPOINTS.getSchedules;
    return this.http.post(url, schedule, { "headers": this.headerOptions });
  }

  updateSchedule(schedule: ScheduleDto) {
    const url = this.REST_API + ENDPOINTS.getSchedules;
    return this.http.put(url, schedule, { "headers": this.headerOptions });
  }

  deleteSchedule(scheduleId: number) {
    const url = this.REST_API + ENDPOINTS.getSchedules + '/' + scheduleId;
    return this.http.delete(url, { "headers": this.headerOptions });
  }

  formatDate(myDate: Date) {

    var yyyy = myDate.getFullYear();
    var mm = myDate.getMonth() < 9 ? "0" + (myDate.getMonth() + 1) : (myDate.getMonth() + 1); // getMonth() is zero-based
    var dd = myDate.getDate() < 10 ? "0" + myDate.getDate() : myDate.getDate();
    return "".concat(mm + '/').concat(dd + '/').concat(yyyy + '');
  }

  getScheduleBySchedulerId(schedulerId: any) {
    const url = this.REST_API + ENDPOINTS.getSchedules + '/' + schedulerId;
    return this.http.get(url, { "headers": this.headerOptions });
  }

  login(accountDto: AccountDto) {
    const url = this.REST_API + ENDPOINTS.urlLogin;
    return this.http.post(url, accountDto, { "headers": header });
  }
  getTokenInLocal() {
    const storage = localStorage.getItem(ACCESS_TOKEN.KEY) as string;
    if (storage === null || storage === '') {
      const token = new LocalStorageDto();
      return token;
    }
    return JSON.parse(storage) as LocalStorageDto;
  }
}
