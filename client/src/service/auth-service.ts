import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { AppService } from "./app-service";
@Injectable()
export class AuthComponentService implements CanActivate {
  constructor(private service: AppService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const isLogin = this.service.getTokenInLocal().isLogin;
    if (!isLogin) {
      window.alert("You need log in!!!");
      this.router.navigate(['/login']);
    }
    return isLogin;
  }

}
