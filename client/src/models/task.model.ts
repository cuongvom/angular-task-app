export class TaskDto {
  constructor() { }
  id: number = 0;
  title: string = '';
  description: string = '';
  statusTask: number = 0;
}
