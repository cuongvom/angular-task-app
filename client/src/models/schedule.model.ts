import { SchedulerDto } from "./scheduler.model";

export class ScheduleDto {
  constructor() { }
  id: number = 0;
  title: string = '';
  description: string = '';
  location: string = '';
  scheduler: SchedulerDto = new SchedulerDto();
  startDate: string = '';
  endDate: string = '';
}
