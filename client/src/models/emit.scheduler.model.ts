import { SchedulerDto } from 'src/models/scheduler.model';
export class EmitSchedulerDto {
  scheduler: SchedulerDto = new SchedulerDto();
  typeAction: string = '';
}
