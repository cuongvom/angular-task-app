import { Component, OnInit, ViewChild } from '@angular/core';
import { TaskDto } from 'src/models/task.model';
import { AppService } from 'src/service/app-service';
import { CATEGORIES } from '../share/constants';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  newTasks: TaskDto[] = [];
  inProgressTasks: TaskDto[] = [];
  doneTasks: TaskDto[] = [];

  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {
    this.service.getAllTask().subscribe(data => {
      var result = data as TaskDto[];
      this.newTasks = result.filter(x => x.statusTask == CATEGORIES.NEW);
      this.inProgressTasks = result.filter(x => x.statusTask == CATEGORIES.IN_PROGRESS);
      this.doneTasks = result.filter(x => x.statusTask == CATEGORIES.DONE);
    });
  }
}
