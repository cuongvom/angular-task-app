import { TaskDto } from './../../../models/task.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryDto } from 'src/models/category.model';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {
  createTaskForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    statusTask: new FormControl('', [Validators.required])
  });
  listCategory: CategoryDto[] = [];
  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {
    this.service.getAllCategory().subscribe(data => {
      this.listCategory = data as CategoryDto[];
    });
  }

  onSubmit() {
    if (this.createTaskForm.valid) {
      const formValue = this.createTaskForm.value as TaskDto;
      this.service.addTask(formValue)
        .subscribe(data => {
          const result = data as TaskDto;
          if (result)
            this.setToDefault();
        });
    }
  }

  goBack(){
    this.service.goBack();
  }

  private setToDefault() {
    this.createTaskForm.reset();
  }
}
