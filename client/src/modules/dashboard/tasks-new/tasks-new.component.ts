import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faList } from '@fortawesome/free-solid-svg-icons';
import { TaskDto } from 'src/models/task.model';
import { CATEGORIES } from 'src/modules/share/constants';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-tasks-new',
  templateUrl: './tasks-new.component.html',
  styleUrls: ['./tasks-new.component.css']
})
export class TasksNewComponent implements OnInit {
  faList = faList;
  title = "Task 1";
  status = "New";
  assignee = "Person 1";
  @Input() listTask: TaskDto[] = [];

  constructor(private routes: ActivatedRoute, private router: Router, private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {

  }

  onClick() {
    this.router.navigate(['/create-task']);
  }
}
