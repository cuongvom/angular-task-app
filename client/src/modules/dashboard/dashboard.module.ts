import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { DashBoardRouting } from "./dashboard.routing";
import { DashboardComponent } from './dashboard.component';
import { TasksInProgressComponent } from './tasks-in-progress/tasks-in-progress.component';
import { TasksDoneComponent } from './tasks-done/tasks-done.component';
import { TasksNewComponent } from './tasks-new/tasks-new.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { CreateTaskComponent } from './create-task/create-task.component';
import { BrowserModule } from "@angular/platform-browser";
import { ViewEditTaskComponent } from './view-edit-task/view-edit-task.component';
import { SpecificTaskComponent } from './specific-task/specific-task.component';

@NgModule(
  {
    imports:[
      DashBoardRouting,
      FontAwesomeModule,
      ReactiveFormsModule,
      BrowserModule
    ],
    declarations:[
      DashboardComponent,
      TasksInProgressComponent,
      TasksDoneComponent,
      TasksNewComponent,
      TaskItemComponent,
      CreateTaskComponent,
      ViewEditTaskComponent,
      SpecificTaskComponent
    ],
    providers:[]
  }
)

export class DashBoardModule{}
