import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { TaskDto } from 'src/models/task.model';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() taskItem: TaskDto = new TaskDto();
  @Input() isDone: boolean = false;
  isShowSpecific: boolean = false;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onClick(taskId: number) {
    if (!this.isDone) {
      this.router.navigate(['/view-edit-task', { id: taskId }]);
    }
    else {
      if (this.isShowSpecific)
        this.isShowSpecific = false;
      else
        this.isShowSpecific = true;
    }
  }

  closeSpecific() {
    this.isShowSpecific = false;
  }
}
