import { Component, Input, OnInit } from '@angular/core';
import { TaskDto } from 'src/models/task.model';
import { CATEGORIES } from 'src/modules/share/constants';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-tasks-done',
  templateUrl: './tasks-done.component.html',
  styleUrls: ['./tasks-done.component.css']
})
export class TasksDoneComponent implements OnInit {
  @Input() listTask: TaskDto[] = [];

  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {

  }
}
