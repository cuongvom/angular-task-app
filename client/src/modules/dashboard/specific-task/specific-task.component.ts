import { Component, Input, OnInit } from '@angular/core';
import { TaskDto } from 'src/models/task.model';

@Component({
  selector: 'app-specific-task',
  templateUrl: './specific-task.component.html',
  styleUrls: ['./specific-task.component.css']
})
export class SpecificTaskComponent implements OnInit {
  @Input() specificTask: TaskDto = new TaskDto();
  constructor() { }

  ngOnInit(): void {
  }

}
