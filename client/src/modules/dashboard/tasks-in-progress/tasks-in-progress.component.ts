import { Component, Input, OnInit } from '@angular/core';
import { TaskDto } from 'src/models/task.model';
import { CATEGORIES } from 'src/modules/share/constants';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-tasks-in-progress',
  templateUrl: './tasks-in-progress.component.html',
  styleUrls: ['./tasks-in-progress.component.css']
})
export class TasksInProgressComponent implements OnInit {
  @Input() listTask: TaskDto[] = [];

  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {

  }
}
