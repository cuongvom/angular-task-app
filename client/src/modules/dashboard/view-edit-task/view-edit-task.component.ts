import { TaskDto } from './../../../models/task.model';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CategoryDto } from 'src/models/category.model';
import { SWITCH_TAG } from 'src/modules/share/constants';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-view-edit-task',
  templateUrl: './view-edit-task.component.html',
  styleUrls: ['./view-edit-task.component.css']
})
export class ViewEditTaskComponent implements OnInit {
  editFormTask = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    statusTask: new FormControl('', [Validators.required])
  });
  listCategory: CategoryDto[] = [];
  isEdit: boolean = false;
  specificTask: TaskDto = new TaskDto;

  constructor(private service: AppService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {
    this.service.getAllCategory().subscribe(data => {
      this.listCategory = data as CategoryDto[];
    });
    const taskId = this.route.snapshot.paramMap.get('id');
    this.service.specificTask(taskId).subscribe(data => {
      this.specificTask = data as TaskDto;
      const defaultData = {
        title: this.specificTask.title,
        description: this.specificTask.description,
        statusTask: this.specificTask.statusTask
      };
      (<FormGroup>this.editFormTask).setValue(defaultData);
    });
  }
  goBack() {
    this.service.goBack();
  }

  onSubmit() {
    if (this.editFormTask) {
      const task = this.editFormTask.value as TaskDto;
      task.id = this.specificTask.id;
      this.service.updateTask(task).subscribe(() => {
        window.alert("Updated");
        this.goBack();
      });
    }
  }

  deleteTask(id: number) {
    const confirm = this.service.confirmDialog("Are you sure?");
    if (confirm) {
      this.service.deleteTask(id).subscribe(() => {
        window.alert("Deleted");
        this.goBack();
      });
    }
  }

  switchTag(switchTag: any) {
    if (switchTag === SWITCH_TAG.VIEW)
      this.isEdit = false;
    else
      this.isEdit = true;
  }
}
