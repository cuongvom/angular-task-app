import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes = [
  {
    path:'',
    component: DashboardComponent
  }
];

export const DashBoardRouting = RouterModule.forChild(routes);
