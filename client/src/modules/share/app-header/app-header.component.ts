import { ACCESS_TOKEN } from './../constants';
import { Component, OnInit } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { LocalStorageDto } from 'src/models/localstorage.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
  faUser = faUser;
  token = localStorage.getItem(ACCESS_TOKEN.KEY);
  tokenDto: LocalStorageDto = new LocalStorageDto();
  isLogin: boolean = false;
  userLogin: string = '';
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.initialData();
  }

  initialData() {
    if (this.token !== null) {
      this.tokenDto = JSON.parse(this.token) as LocalStorageDto;
      this.isLogin = this.tokenDto.isLogin;
      this.userLogin = this.tokenDto.userN;
    }
    else {
      this.isLogin = false;
    }
  }

  logOut() {
    this.isLogin = false;
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
