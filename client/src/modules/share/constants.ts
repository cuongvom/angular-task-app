import { HttpHeaders } from "@angular/common/http";

export enum ENDPOINTS {
  getTasks = 'api/tasks',
  getSchedules = 'api/schedule',
  getSchedulers = 'api/scheduler',
  getCategories = 'api/categories',
  urlLogin = 'api/users/login'
}

export enum CATEGORIES {
  NEW = 1,
  IN_PROGRESS = 2,
  DONE = 3
}

export const header = new HttpHeaders()
  .set('content-type', 'application/json')
  .set('Access-Control-Allow-Origin', '*');

export enum SWITCH_TAG {
  VIEW = 'VIEW',
  EDIT = 'EDIT'
}

export enum TYPE_ACTION {
  EDIT = 'EDIT',
  CREATE = 'CREATE',
  DELETE = 'DELETE',
  CANCEL = 'CANCEL'
}

export enum ACCESS_TOKEN {
  KEY = "ACCESS_TOKEN"
}
