import { EmitSchedulerDto } from './../../models/emit.scheduler.model';
import { TYPE_ACTION } from './../share/constants';
import { Component, OnInit } from '@angular/core';
import { SchedulerDto } from 'src/models/scheduler.model';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-app-users',
  templateUrl: './app-users.component.html',
  styleUrls: ['./app-users.component.css']
})
export class AppUsersComponent implements OnInit {

  listScheduler: SchedulerDto[] = [];
  isCreate: boolean = false;
  userAction: string = "Add";
  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }
  initialData() {
    this.service.getAllScheduler().subscribe(data => {
      this.listScheduler = data as SchedulerDto[];
    });
  }

  addUser() {
    if (this.isCreate) {
      this.userAction = "Add";
      this.isCreate = false;
    }
    else {
      this.userAction = "Cancel";
      this.isCreate = true;
    }
  }

  reloadScheduler(event: any) {
    if (event) {
      const result = event as EmitSchedulerDto;
      if (TYPE_ACTION.CREATE === result.typeAction)
        this.initialData();
      if (TYPE_ACTION.EDIT === result.typeAction) {
        this.initialData();
      }
      if (TYPE_ACTION.DELETE === result.typeAction) {
        this.initialData();
      }
      this.listScheduler.sort();
    }
  }
}
