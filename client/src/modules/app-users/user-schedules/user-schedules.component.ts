import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScheduleDto } from 'src/models/schedule.model';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-user-schedules',
  templateUrl: './user-schedules.component.html',
  styleUrls: ['./user-schedules.component.css']
})
export class UserSchedulesComponent implements OnInit {
  listSchedule: ScheduleDto[] = [];
  constructor(private service: AppService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initialData();
  }
  initialData() {
    const schedulerId = this.route.snapshot.paramMap.get('schedulerId');
    this.service.getScheduleBySchedulerId(schedulerId).subscribe(data => {
      this.listSchedule = data as ScheduleDto[];
    });
  }
  goBack(){
    this.service.goBack();
  }
}
