import { RouterModule } from '@angular/router';
import { AppUsersComponent } from './app-users.component';
const routes = [
  {
    path: '',
    component: AppUsersComponent
  }
];

export const AppUserRouting = RouterModule.forChild(routes);
