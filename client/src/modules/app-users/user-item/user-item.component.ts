import { Router } from '@angular/router';
import { TYPE_ACTION } from './../../share/constants';
import { EmitSchedulerDto } from './../../../models/emit.scheduler.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SchedulerDto } from 'src/models/scheduler.model';
import { AppService } from 'src/service/app-service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {

  isEdit: boolean = false;
  editUserForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    position: new FormControl('', [Validators.required])
  });
  @Input() schedulerItem: SchedulerDto = new SchedulerDto();
  @Input() isCreate: boolean = false;
  @Output() reload = new EventEmitter<EmitSchedulerDto>();
  constructor(private service: AppService, private router: Router) { }

  ngOnInit(): void {
  }

  changeToEdit() {
    if (this.isEdit)
      this.isEdit = false;
    else
      this.isEdit = true;
  }

  onSubmit() {
    if (this.editUserForm.valid) {
      const scheduler = this.editUserForm.value as SchedulerDto;
      if (this.isCreate) {
        this.service.addScheduler(scheduler).subscribe(data => {
          const result = data as SchedulerDto;
          if (result !== null) {
            this.editUserForm.reset();
            this.reloadParent(TYPE_ACTION.CREATE, result);
          }
        });
      }
      if (this.isEdit) {
        scheduler.id = this.schedulerItem.id;
        this.service.updateScheduler(scheduler).subscribe(data => {
          const result = data as SchedulerDto;
          if (result !== null) {
            this.editUserForm.reset();
            this.isEdit = false;
            this.reloadParent(TYPE_ACTION.EDIT, result);
          }
        });
      }
    }
  }

  deleteUser(scheduleId: number) {
    var confirm = this.service.confirmDialog("Are you sure?");
    if (confirm) {
      this.service.deleteScheduler(scheduleId).subscribe(() => {
        window.alert("Deleted");
        this.reloadParent(TYPE_ACTION.DELETE, this.schedulerItem);
      });
    }
  }

  reloadParent(typeEmit: any, scheduler: SchedulerDto) {
    const emitDto = new EmitSchedulerDto();
    emitDto.scheduler = scheduler;
    emitDto.typeAction = typeEmit;
    this.reload.emit(emitDto);
  }

  viewSchedules(schedulerId: any) {
    this.router.navigate(['/schedules', { schedulerId: schedulerId }]);
  }
}
