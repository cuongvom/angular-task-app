import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AppUserRouting } from "./app-users.routing";
import { AppUsersComponent } from "./app-users.component";
import { UserItemComponent } from './user-item/user-item.component';
import { UserSchedulesComponent } from './user-schedules/user-schedules.component';

@NgModule(
  {
    imports:[
      AppUserRouting,
      ReactiveFormsModule,
      BrowserModule
    ],
    declarations:[
      AppUsersComponent,
      UserItemComponent,
      UserSchedulesComponent
    ],
    providers:[]
  }
)

export class AppUserModule{}
