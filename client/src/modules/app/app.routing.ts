import { Routes } from '@angular/router';
import { AppUsersComponent } from '../app-users/app-users.component';
import { AppScheduleComponent } from '../app-schedule/app-schedule.component';
import { DashboardComponent } from './../dashboard/dashboard.component';
import { CreateTaskComponent } from '../dashboard/create-task/create-task.component';
import { ViewEditTaskComponent } from '../dashboard/view-edit-task/view-edit-task.component';
import { UserSchedulesComponent } from '../app-users/user-schedules/user-schedules.component';
import { AccountLoginComponent } from './account-login/account-login.component';
import { AuthComponentService } from 'src/service/auth-service';

export const AppRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DashboardComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'user',
    component: AppUsersComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'schedule',
    component: AppScheduleComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'create-task',
    component: CreateTaskComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'view-edit-task',
    component: ViewEditTaskComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'schedules',
    component: UserSchedulesComponent,
    canActivate: [AuthComponentService]
  },
  {
    path: 'login',
    component: AccountLoginComponent
  }
]
