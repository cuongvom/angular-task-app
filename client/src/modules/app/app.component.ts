import { ACCESS_TOKEN } from './../share/constants';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task-project';
  token = localStorage.getItem(ACCESS_TOKEN.KEY);
}
