import { DashBoardModule } from './../dashboard/dashboard.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IntlModule } from "@progress/kendo-angular-intl";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { LabelModule } from "@progress/kendo-angular-label";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";

import { AppRoutingModule } from './app-routing.module';
import { AppRoutes } from './app.routing';

import { AppComponent } from './app.component';
import { AppFooterComponent } from '../share/app-footer/app-footer.component';
import { AppHeaderComponent } from '../share/app-header/app-header.component';
import { AppService } from 'src/service/app-service';
import { AppUserModule } from '../app-users/app-users.module';
import { AppScheduleModule } from '../app-schedule/app-schedule.module';
import { AccountLoginComponent } from './account-login/account-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthComponentService } from 'src/service/auth-service';

library.add(faPlus);

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AccountLoginComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    DashBoardModule,
    AppUserModule,
    AppScheduleModule,
    RouterModule.forRoot(AppRoutes, {
      relativeLinkResolution: 'legacy'
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    IntlModule,
    LabelModule,
    DateInputsModule,
    DropDownsModule,
    ReactiveFormsModule
  ],
  providers: [
    AppService,
    AuthComponentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
