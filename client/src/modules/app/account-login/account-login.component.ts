import { ACCESS_TOKEN } from './../../share/constants';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountDto } from 'src/models/account.model';
import { AppService } from 'src/service/app-service';
import { LocalStorageDto } from 'src/models/localstorage.model';

@Component({
  selector: 'app-account-login',
  templateUrl: './account-login.component.html',
  styleUrls: ['./account-login.component.css']
})
export class AccountLoginComponent implements OnInit {

  formLogin: FormGroup = new FormGroup({
    userName: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(private service: AppService, private router: Router) {
  }

  ngOnInit(): void {
  }
  onSubmit() {
    if (this.formLogin.valid) {
      const data = this.formLogin.value as AccountDto;
      this.service.login(data).subscribe(res => {
        const token = res as any;
        if (token.token) {
          const tokenDto = new LocalStorageDto();
          tokenDto.token = token.token;
          tokenDto.userN = data.userName;
          tokenDto.isLogin = true;
          localStorage.setItem(ACCESS_TOKEN.KEY, JSON.stringify(tokenDto));
          this.router.navigate(['/dashboard']).then(() => {
            window.location.reload();
          });
        }
        else {
          window.alert("User name or password invalid");
        }
      });
    }
  }
}
