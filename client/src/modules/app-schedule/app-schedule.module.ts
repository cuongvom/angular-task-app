import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IntlModule } from "@progress/kendo-angular-intl";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { LabelModule } from "@progress/kendo-angular-label";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { AppScheduleComponent } from "./app-schedule.component";
import { AppScheduleRouting } from "./app-schedule.routing";
import { ScheduleItemComponent } from './schedule-item/schedule-item.component';

@NgModule(
  {
    imports: [
      AppScheduleRouting,
      ReactiveFormsModule,
      BrowserModule,
      BrowserAnimationsModule,
      IntlModule,
      LabelModule,
      DateInputsModule,
      DropDownsModule
    ],
    declarations: [
      AppScheduleComponent,
      ScheduleItemComponent
    ],
    providers: []
  }
)

export class AppScheduleModule { }
