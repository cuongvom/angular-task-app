import { Component, Input, OnInit } from '@angular/core';
import { ScheduleDto } from 'src/models/schedule.model';

@Component({
  selector: 'app-schedule-item',
  templateUrl: './schedule-item.component.html',
  styleUrls: ['./schedule-item.component.css']
})
export class ScheduleItemComponent implements OnInit {
  @Input() isEdit: boolean = false;
  @Input() isCreate: boolean = false;
  @Input() scheduleItem: ScheduleDto = new ScheduleDto();
  constructor() { }

  ngOnInit(): void {
  }
}
