import { RouterModule } from '@angular/router';
import { AppScheduleComponent } from './app-schedule.component';

const routes = [
  {
    path:'',
    component: AppScheduleComponent
  }
];

export const AppScheduleRouting = RouterModule.forChild(routes);
