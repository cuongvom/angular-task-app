import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/service/app-service';
import { Component, OnInit } from '@angular/core';
import { ScheduleDto } from 'src/models/schedule.model';
import { TYPE_ACTION } from '../share/constants';
import { SchedulerDto } from 'src/models/scheduler.model';

@Component({
  selector: 'app-app-schedule',
  templateUrl: './app-schedule.component.html',
  styleUrls: ['./app-schedule.component.css']
})
export class AppScheduleComponent implements OnInit {

  listSchedule: ScheduleDto[] = [];
  listScheduler: SchedulerDto[] = [];
  isEdit: boolean = false;
  isCreate: boolean = false;
  startDate: Date = new Date();
  endDate: Date = new Date();
  actionCreate: string = 'Add';
  actionEdit: string = 'Edit';
  scheduleCreateForm: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    scheduler: new FormControl(new SchedulerDto(), [Validators.required]),
    location: new FormControl('', [Validators.required]),
  });
  scheduleEditForm: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    scheduler: new FormControl(new SchedulerDto(), [Validators.required]),
    location: new FormControl('', [Validators.required]),
  });
  scheduleIdSelected: number = 0;
  constructor(private service: AppService) { }

  ngOnInit(): void {
    this.initialData();
  }
  initialData() {
    this.service.getAllSchedule().subscribe(data => {
      this.listSchedule = data as ScheduleDto[];
    });
    this.service.getAllScheduler().subscribe(data => {
      this.listScheduler = data as SchedulerDto[];
    });
  }
  changeAction(typeAction: any, defaultStartDate: any, defaultEndDate: any, scheduleId: number) {
    if (TYPE_ACTION.EDIT === typeAction) {
      this.scheduleIdSelected = scheduleId;
    }
    if(TYPE_ACTION.CANCEL === typeAction){
      this.scheduleIdSelected = 0;
      this.startDate = new Date(defaultStartDate);
        this.endDate = new Date(defaultEndDate);
    }
    if (TYPE_ACTION.CREATE === typeAction) {
      if (this.isCreate) {
        this.isCreate = false;
        this.actionCreate = "Add"
      }
      else {
        this.isCreate = true;
        this.actionCreate = "Cancel";
        this.startDate = new Date();
        this.endDate = new Date();
      }
    }
  }
  onSubmit(actionType: any) {
    if (TYPE_ACTION.EDIT === actionType) {
      const schedule = this.scheduleEditForm.value as ScheduleDto;
      schedule.startDate = this.service.formatDate(this.startDate);
      schedule.endDate = this.service.formatDate(this.endDate);
      schedule.id = this.scheduleIdSelected;
      this.service.updateSchedule(schedule).subscribe(data => {
        const result = data as ScheduleDto;
        if (result != null) {
          this.initialData();
          this.scheduleIdSelected = 0;
          window.alert("Updated");
        }
      });
    }
    if (TYPE_ACTION.CREATE === actionType) {
      if (this.scheduleCreateForm.valid) {
        const schedule = this.scheduleCreateForm.value as ScheduleDto;
        schedule.startDate = this.service.formatDate(this.startDate);
        schedule.endDate = this.service.formatDate(this.endDate);
        this.service.addSchedule(schedule).subscribe(data => {
          const result = data as ScheduleDto;
          if (result != null) {
            this.initialData();
            this.scheduleCreateForm.reset();
            this.startDate = new Date();
            this.endDate = new Date();
          }
        });
      }
    }
  }
  deleteSchedule(scheduleId: number) {
    const confirm = this.service.confirmDialog("Are you sure?");
    if (confirm) {
      this.service.deleteSchedule(scheduleId).subscribe(() => {
        window.alert("Deleted");
        this.initialData();
      });
    }
  }
}
