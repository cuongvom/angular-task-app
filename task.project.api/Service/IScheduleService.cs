using System.Collections.Generic;
using System.Threading.Tasks;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public interface IScheduleService
    {
        public Task<List<ScheduleDto>> GetAllAsync();
        public Task<List<ScheduleDto>> GetAllBySchedulerIdAsync(int schedulerId);
        public Task DeleteSchedule(int scheduleId);
        public Task<ScheduleDto> AddSchedule(ScheduleDto scheduleDto);
        public Task<ScheduleDto> UpdateSchedule(ScheduleDto scheduleDto);
    }
}