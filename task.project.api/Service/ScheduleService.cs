using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using task.project.api.Context;
using task.project.api.Domain.Dto;
using task.project.api.Domain.Entities;

namespace task.project.api.Service
{
    public class ScheduleService : IScheduleService
    {
        private readonly TaskProjectDbContext _context;
        private readonly IMapper _mapper;
        public ScheduleService(TaskProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ScheduleDto> AddSchedule(ScheduleDto scheduleDto)
        {
            var schedule = new ScheduleEntity()
            {
                Title = scheduleDto.Title,
                Description = scheduleDto.Description,
                Creator = scheduleDto.Scheduler.Name,
                StartDate = scheduleDto.StartDate,
                EndDate = scheduleDto.EndDate,
                SchedulerId = scheduleDto.Scheduler.Id,
                Location = scheduleDto.Location
            };
            await _context.Schedules.AddAsync(schedule);
            _context.SaveChanges();
            scheduleDto.Id = schedule.Id;
            return scheduleDto;
        }

        public async Task DeleteSchedule(int scheduleId)
        {
            var schedule = await _context.Schedules.FirstOrDefaultAsync(x => x.Id == scheduleId);
            _context.Schedules.Remove(schedule);
            _context.SaveChanges();
        }

        public async Task<List<ScheduleDto>> GetAllAsync()
        {
            var schedules = await _context.Schedules.ToListAsync();
            return _mapper.Map<List<ScheduleDto>>(schedules);
        }

        public async Task<List<ScheduleDto>> GetAllBySchedulerIdAsync(int schedulerId)
        {
            var schedules = await _context.Schedules.Where(x => x.SchedulerId == schedulerId).ToListAsync();
            return _mapper.Map<List<ScheduleDto>>(schedules);
        }

        public async Task<ScheduleDto> UpdateSchedule(ScheduleDto scheduleDto)
        {
            var schedule = await _context.Schedules.FirstOrDefaultAsync(x => x.Id == scheduleDto.Id);
            schedule.Title = scheduleDto.Title;
            schedule.Description = scheduleDto.Description;
            schedule.Creator = scheduleDto.Scheduler.Name;
            schedule.StartDate = scheduleDto.StartDate;
            schedule.EndDate = scheduleDto.EndDate;
            schedule.Location = scheduleDto.Location;
            schedule.SchedulerId = scheduleDto.Scheduler.Id;
            _context.Entry(schedule).State = EntityState.Modified;
            _context.SaveChanges();
            scheduleDto.Id = schedule.Id;
            return scheduleDto;
        }
    }
}