using System.Threading.Tasks;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public interface IUserService
    {
        public Task<string> GetTokenAsync(AccountDto accountDto);
    }
}