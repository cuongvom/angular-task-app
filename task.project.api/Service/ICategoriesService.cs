using System.Collections.Generic;
using System.Threading.Tasks;
using task.project.api.Domain.Dto;

namespace task.project.api.Service
{
    public interface ICategoryService
    {
        public Task<List<CategoryDto>> GetCategoryAsync();
    }
}