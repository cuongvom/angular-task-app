using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using task.project.api.Context;
using task.project.api.Domain.Dto;
using Microsoft.Extensions.Configuration;
namespace task.project.api.Service
{
    public class UserService : IUserService
    {
        private readonly TaskProjectDbContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UserService(TaskProjectDbContext context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<string> GetTokenAsync(AccountDto accountDto)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == new Guid("4dce0d91-e845-4181-beef-03474fa53823"));
            if (user.UserName == accountDto.UserName && user.Password == accountDto.Password)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("TokenOptions:SigningKey")));
                var signInCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                var tokeOptions = new JwtSecurityToken(
                    issuer: _configuration.GetValue<string>("TokenOptions:Issuer"),
                    expires: DateTime.Now.AddDays(_configuration.GetValue<int>("TokenOptions:LifeTime")),
                    audience: _configuration.GetValue<string>("TokenOptions:Audience"),
                    signingCredentials: signInCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return tokenString;
            }
            return string.Empty;
        }
    }
}