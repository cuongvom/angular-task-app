using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using task.project.api.Context;
using task.project.api.Domain.Dto;
using task.project.api.Domain.Entities;

namespace task.project.api.Service
{
    public class SchedulerService : ISchedulerService
    {
        private readonly TaskProjectDbContext _context;
        private readonly IMapper _mapper;
        public SchedulerService(TaskProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<SchedulerDto> AddSchedulerAsync(SchedulerDto schedulerDto)
        {
            var scheduler = new SchedulerEntity()
            {
                Name = schedulerDto.Name,
                Position = schedulerDto.Position
            };
            await _context.Schedulers.AddAsync(scheduler);
            _context.SaveChanges();
            return _mapper.Map<SchedulerDto>(schedulerDto);
        }

        public async Task DeleteSchedulerAsync(int schedulerId)
        {
            var scheduler = await _context.Schedulers.FirstOrDefaultAsync(x => x.Id == schedulerId);
            _context.Schedulers.Remove(scheduler);
            _context.SaveChanges();
        }

        public async Task<List<SchedulerDto>> GetAllAsync()
        {
            var schedulers = await _context.Schedulers.ToListAsync();
            return _mapper.Map<List<SchedulerDto>>(schedulers);
        }

        public async Task<SchedulerDto> UpdateSchedulerAsync(SchedulerDto schedulerDto)
        {
            var scheduler = await _context.Schedulers.FirstOrDefaultAsync(x => x.Id == schedulerDto.Id);
            scheduler.Name = schedulerDto.Name;
            scheduler.Position = schedulerDto.Position;
            _context.Entry(scheduler).State = EntityState.Modified;
            _context.SaveChanges();
            return _mapper.Map<SchedulerDto>(scheduler);
        }
    }
}