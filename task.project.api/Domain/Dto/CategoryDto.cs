namespace task.project.api.Domain.Dto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}