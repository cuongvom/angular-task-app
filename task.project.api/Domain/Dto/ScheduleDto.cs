using System;

namespace task.project.api.Domain.Dto
{
    public class ScheduleDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public SchedulerDto Scheduler { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}