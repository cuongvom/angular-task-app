using System;
using System.Collections.Generic;

namespace task.project.api.Domain.Entities
{
    public class UserEntity : BaseEntity<Guid>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid RoleId { get; set; }
        public RoleEntity Role { get; set; }
    }
}