using System;
namespace task.project.api.Domain.Entities
{
    public class TaskEntity : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public CategoryEntity Category { get; set; }
    }
}