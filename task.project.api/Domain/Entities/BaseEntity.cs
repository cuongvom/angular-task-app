using System.ComponentModel.DataAnnotations;

namespace task.project.api.Domain.Entities
{
    public abstract class BaseEntity<T>
    {
        [Key]
        public T Id { get; set; }
    }
}