using AutoMapper;
using task.project.api.Domain.Dto;
using task.project.api.Domain.Entities;

namespace task.project.api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CategoryEntity, CategoryDto>();

            CreateMap<TaskEntity, TaskDto>()
            .ForMember(dis => dis.StatusTask, inp => inp.MapFrom(x => x.CategoryId));

            CreateMap<TaskDto, TaskEntity>()
            .ForMember(dis => dis.CategoryId, e => e.MapFrom(x => x.StatusTask));

            CreateMap<UserEntity, UserDto>();

            CreateMap<SchedulerEntity, SchedulerDto>();

            CreateMap<ScheduleEntity, ScheduleDto>()
            .ForMember(dis => dis.Scheduler, inp => inp.MapFrom(x => new SchedulerDto(){
                Name = x.Creator,
                Id = x.SchedulerId
            }));
        }
    }
}