using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using task.project.api.Domain.Dto;
using task.project.api.Service;

namespace task.project.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class SchedulerController : ControllerBase
    {
        private readonly ISchedulerService _service;
        public SchedulerController(ISchedulerService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _service.GetAllAsync();
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody] SchedulerDto schedulerDto)
        {
            var result = await _service.AddSchedulerAsync(schedulerDto);
            return Created("",result);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] SchedulerDto schedulerDto)
        {
            var result = await _service.UpdateSchedulerAsync(schedulerDto);
            return Accepted(result);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteSchedulerAsync(id);
            return Ok();
        }
    }
}